const Tag = require("../models/tag")
const WholeSeller = require("../models/wholeseller")
const Retailer = require("../models/retailer")
const Customer = require("../models/customer")
const TagTransaction = require("../models/tag_transaction")

const { catchAsync } = require("../utils/Helper")

TagTransaction.belongsTo(Tag, { foreignKey: { name: 'tag_id' } });
TagTransaction.belongsTo(WholeSeller, { foreignKey: { name: 'wholeseller_id' } });
TagTransaction.belongsTo(Retailer, { foreignKey: { name: 'retailer_id' } });
TagTransaction.belongsTo(Customer, { foreignKey: { name: 'customer_id' } });


/**
 * Get All Transactions
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Transactions Data Object
 */
module.exports.getTransactions = catchAsync(async (req, res, next) => {
    const transactions = await TagTransaction.findAll({
        where: { wholeseller_id: req.user_id, customer_id: null },
        // include: {
        //     model: WholeSeller, as: 'whole_seller'
        //     //attributes: ['name']
        //     //where: { name: 'Samir' }
        // }
    })
    return res.status(200).json({
        success: true,
        status: "success",
        transactions,
        message: "Transactions fetched succesfully"
    });
})

/**
 * Get Transactions Details
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Transactions Data Object
 */
module.exports.getTransactionsDetails = catchAsync(async (req, res, next) => {
    // const customer = await Transactions.findOne({ where: { id: req.params.vehicle_type_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        //customer,
        message: "VehicalType fetched succesfully"
    });
})