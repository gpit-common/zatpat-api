const VehicalType = require("../models/vehicle-type")
const Tag = require("../models/tag")
const { check, validationResult } = require("express-validator");
const { catchAsync } = require("../utils/Helper")

const tableKeys = ["name", "details"]

VehicalType.hasMany(Tag, { foreignKey: { name: 'vehicle_type_id' } });

/**
 * Get All VehicalTypes
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} VehicalTypes Data Object
 */
module.exports.getVehicalTypes = catchAsync(async (req, res, next) => {
    const customers = await VehicalType.findAll({ where: { wholeseller_id: req.user_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        customers,
        message: "VehicalTypes fetched succesfully"
    });
})

/**
 * Get VehicalType Details
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} VehicalType Data Object
 */
module.exports.getVehicalTypeDetails = catchAsync(async (req, res, next) => {
    const customer = await VehicalType.findOne({ where: { id: req.params.vehicle_type_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        customer,
        message: "VehicalType fetched succesfully"
    });
})

/**
 * Create VehicalType
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Created VehicalType Data
 */
module.exports.addVehicalType = catchAsync(async (req, res, next) => {
    /**
	 * Validate Input Data
	 */
    await check("name", "Name Is Required").exists().run(req);
    await check("status", "Status Is Required").exists().run(req);

    const result = validationResult(req);
    if (!result.isEmpty()) return res.status(422).json({ status: "fail", success: false, errors: result.array() });
    await VehicalType.create({
        name: req.body.name,
        status: req.body.status,
        details: req.body.details,
        wholeseller_id: req.user_id,
    })
    return res.status(201).json({
        success: true,
        status: "success",
        message: `VehicalType created succesfully`
    });
})

/**
 * Update VehicalType
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Updated VehicalType Data
 */
module.exports.updateVehicalType = catchAsync(async (req, res, next) => {
    vehicleTypeData = {}
    /** Data Prepare */
    if (req.body.name) {
        vehicleTypeData['name'] = req.body.name
    }
    if (req.body.status) {
        vehicleTypeData['status'] = req.body.status
    }
    if (req.body.details) {
        vehicleTypeData['details'] = req.body.details
    }
    await VehicalType.update(vehicleTypeData, { where: { id: req.params.vehicle_type_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        message: `VehicalType updated succesfully`
    });
})

/**
 * Delete VehicalType
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 */
module.exports.deleteVehicalType = catchAsync(async (req, res, next) => {
    await VehicalType.destroy({ where: { id: req.params.vehicle_type_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        message: `VehicalType Deleted Successfully`
    });
})