const Bank = require("../models/bank")
const { check, validationResult } = require("express-validator");
const { catchAsync } = require("../utils/Helper")

const tableKeys = ["name", "details"]

/**
 * Get All Banks
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Banks Data Object
 */
module.exports.getBanks = catchAsync(async (req, res, next) => {
    const Banks = await Bank.findAll({ where: { user_id: req.user_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        Banks,
        message: "Banks fetched succesfully"
    });
})

/**
 * Get Bank Details
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Bank Data Object
 */
module.exports.getBankDetails = catchAsync(async (req, res, next) => {
    const bank = await Bank.findOne({ where: { id: req.params.bank_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        bank,
        message: "Bank fetched succesfully"
    });
})

/**
 * Create Bank
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Created Bank Data
 */
module.exports.addBank = catchAsync(async (req, res, next) => {
    /**
	 * Validate Input Data
	 */
    await check("name", "Name Is Required").exists().run(req);
    await check("status", "Status Is Required").exists().run(req);

    const result = validationResult(req);
    if (!result.isEmpty()) return res.status(422).json({ status: "fail", success: false, errors: result.array() });
    await Bank.create({
        name: req.body.name,
        status: req.body.status,
        details: req.body.details,
        user_id: req.user_id,
    })
    return res.status(201).json({
        success: true,
        status: "success",
        message: `Bank created succesfully`
    });
})

/**
 * Update Bank
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Updated Bank Data
 */
module.exports.updateBank = catchAsync(async (req, res, next) => {
    bankData = {}
    /** Data Prepare */
    if (req.body.name) {
        bankData['name'] = req.body.name
    }
    if (req.body.status) {
        bankData['status'] = req.body.status
    }
    if (req.body.details) {
        bankData['details'] = req.body.details
    }
    await Bank.update(bankData, { where: { id: req.params.bank_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        message: `Bank updated succesfully`
    });
})

/**
 * Delete Bank
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 */
module.exports.deleteBank = catchAsync(async (req, res, next) => {
    await Bank.destroy({ where: { id: req.params.bank_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        message: `Bank Deleted Successfully`
    });
})