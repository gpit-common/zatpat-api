const Tag = require("../models/tag")
const DataBase = require("../models/index")
const TagTransaction = require("../models/tag_transaction")
const VehicalType = require("../models/vehicle-type")
const { check, validationResult } = require("express-validator");
const { catchAsync } = require("../utils/Helper")

const tableKeys = ["name", "details"]

Tag.belongsTo(VehicalType, { foreignKey: { name: 'vehicle_type_id' } });

/**
 * Get All Tags
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Tags Data Object
 */
module.exports.getTags = catchAsync(async (req, res, next) => {
    const tags = await Tag.findAll({ where: { wholeseller_id: req.user_id } })
    //const tags = await Tag.findAll({ where: { user_id: req.user_id }, include: [{ model: VehicalType }] })
    return res.status(200).json({
        success: true,
        status: "success",
        tags,
        message: "Tags fetched succesfully"
    });
})

/**
 * Get Tag Details
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Tag Data Object
 */
module.exports.getTagDetails = catchAsync(async (req, res, next) => {
    const tag = await Tag.findOne({ where: { id: req.params.tag_id }, include: [{ model: VehicalType }] })
    return res.status(200).json({
        success: true,
        status: "success",
        tag,
        message: "Tag fetched succesfully"
    });
})

/**
 * Create Tag
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Created Tag Data
 */
module.exports.addTag = async (req, res, next) => {
    /**
	 * Validate Input Data
	 */
    await check("vehicle_type", "Vehicle Type ID Is Required").exists().run(req);
    await check("vehicle_type", "Vehicle Type ID Must Be a Number").isNumeric().run(req);
    await check("count", "Tag Count Is Required").exists().run(req);
    await check("count", "Tag Count Must Be a Number").isNumeric().run(req);

    const result = validationResult(req);
    if (!result.isEmpty()) return res.status(422).json({ status: "fail", success: false, errors: result.array() });
    let count = parseInt(req.body.count, 10)
    if (count <= 0) return res.status(200).json({ success: true, status: "success", message: `Tags Count Must be Greater than zero` });

    const transaction = await DataBase.transaction();
    try {

        for (let index = 1; index <= count; index++) {
            let tag = await Tag.create({
                name: req.body.name,
                status: req.body.status,
                details: req.body.details,
                wholeseller_id: req.user_id,
                vehicle_type_id: req.body.vehicle_type,
            }, { transaction })
            let tag_transaction = await TagTransaction.create({
                wholeseller_id: req.user_id,
                tag_id: tag.id
            }, { transaction })
        }
        await transaction.commit()
        return res.status(200).json({
            success: true,
            status: "success",
            message: `Tags Created succesfully`
        });
    } catch (error) {
        transaction.rollback()
        next(error)
    }
}

/**
 * Update Tag
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 * 
 * @returns {Object} Updated Tag Data
 */
module.exports.updateTag = catchAsync(async (req, res, next) => {
    tagData = {}
    /** Data Prepare */
    if (req.body.name) {
        tagData['name'] = req.body.name
    }
    if (req.body.status) {
        tagData['status'] = req.body.status
    }
    if (req.body.details) {
        tagData['details'] = req.body.details
    }
    if (req.body.vehicle_type) {
        tagData['vehicle_type_id'] = req.body.vehicle_type
    }
    if (req.body.tag_code) {
        tagData['tag_code'] = req.body.tag_code
    }
    await Tag.update(tagData, { where: { id: req.params.tag_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        message: `Tag updated succesfully`
    });
})

/**
 * Delete Tag
 * 
 * @param {Object} req Express Request Object
 * @param {Object} res Express Response Object
 * @param {Object} next Express Request Next handler Function.
 */
module.exports.deleteTag = catchAsync(async (req, res, next) => {
    await Tag.destroy({ where: { id: req.params.tag_id } })
    return res.status(200).json({
        success: true,
        status: "success",
        message: `Tag Deleted Successfully`
    });
})