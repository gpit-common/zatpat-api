const router = require("express").Router();
const auth = require("../controllers/AuthController");
const vehicleType = require("../controllers/VehicleTypeController");

/**
 * Add VehicleType
 */
router.post("/", auth.isUserLoggedIn, vehicleType.addVehicalType)

/**
 * Get Customers
 */
router.get("/", auth.isUserLoggedIn, vehicleType.getVehicalTypes)

/**
 * Get VehicleType Details
 */
router.get("/:vehicle_type_id", auth.isUserLoggedIn, vehicleType.getVehicalTypeDetails)

/**
 * update VehicleType
 */
router.patch("/:vehicle_type_id", auth.isUserLoggedIn, vehicleType.updateVehicalType)

/**
 * Delete VehicleType
 */
router.delete("/:vehicle_type_id", auth.isUserLoggedIn, vehicleType.deleteVehicalType)

module.exports = router;