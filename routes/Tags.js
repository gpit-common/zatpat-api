const router = require("express").Router();
const auth = require("../controllers/AuthController");
const tag = require("../controllers/TagController");

/**
 * Add Tag
 */
router.post("/", auth.isUserLoggedIn, tag.addTag)

/**
 * Get Customers
 */
router.get("/", auth.isUserLoggedIn, tag.getTags)

/**
 * Get Tag Details
 */
router.get("/:tag_id", auth.isUserLoggedIn, tag.getTagDetails)

/**
 * update Tags
 */
router.patch("/:tag_id", auth.isUserLoggedIn, tag.updateTag)

/**
 * Delete Tags
 */
router.delete("/:tag_id", auth.isUserLoggedIn, tag.deleteTag)

module.exports = router;