const router = require("express").Router();
const authController = require("../controllers/AuthController");
const controller = require("../controllers/TagTransactionController");

/**
 * Get Whole Sellers
 */
router.get("/", authController.isUserLoggedIn, controller.getTransactions)

/**
 * Get Whole Seller Details
 */
router.get("/:transaction_id", authController.isUserLoggedIn, controller.getTransactionsDetails)


module.exports = router;