const router = require("express").Router();
const auth = require("../controllers/AuthController");
const banks = require("../controllers/BankController");

/**
 * Add bank
 */
router.post("/", auth.isUserLoggedIn, banks.addBank)

/**
 * Get banks
 */
router.get("/", auth.isUserLoggedIn, banks.getBanks)

/**
 * Get bank Details
 */
router.get("/:bank_id", auth.isUserLoggedIn, banks.getBankDetails)

/**
 * update banks
 */
router.patch("/:bank_id", auth.isUserLoggedIn, banks.updateBank)

/**
 * Delete banks
 */
router.delete("/:bank_id", auth.isUserLoggedIn, banks.deleteBank)

module.exports = router;