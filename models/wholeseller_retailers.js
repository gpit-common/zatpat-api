const { DataTypes } = require('sequelize')
const DataBase = require('./index')

const WholesellerRetailer = DataBase.define('wholeseller_retailers', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    wholesellerId: {
        type: DataTypes.INTEGER
    },
    retailerId: {
        type: DataTypes.STRING
    }
}, {
    timestamps: false,
})
module.exports = WholesellerRetailer