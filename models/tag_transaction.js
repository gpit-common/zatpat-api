const { DataTypes } = require('sequelize')
const DataBase = require('./index')

const TagTransaction = DataBase.define('tag_transactions', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    wholeseller_id: DataTypes.INTEGER,
    retailer_id: DataTypes.INTEGER,
    tag_id: DataTypes.INTEGER,
    customer_id: DataTypes.INTEGER,
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    }
}, { timestamps: false })

module.exports = TagTransaction