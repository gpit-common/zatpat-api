const { DataTypes } = require('sequelize')
const DataBase = require('./index')

const Tag = DataBase.define('tags', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    wholeseller_id: DataTypes.INTEGER,
    vehicle_type_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    tag_code: DataTypes.STRING,
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    details: {
        type: DataTypes.STRING,
    },
    status: {
        type: DataTypes.ENUM('ACTIVE', 'INACTIVE'),
        defaultValue: "INACTIVE"
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    }
}, { timestamps: false })

module.exports = Tag